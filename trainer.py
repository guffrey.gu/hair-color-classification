import numpy as np
import csv
import cv2
import os
import random
import time
import torch
from torch.utils.data.dataset import Dataset
import torch.nn as nn
import torch.nn.functional as F
import torchvision.models as models
from imgaug_transform import *

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class ImageDataset(Dataset):
    def __init__(self, csvPath, isAug):
        super().__init__()
        self.imgList = []
        self.labelList = []
        imgPath = []
        labels = []
        self.transform = ImgAugTransform()

        with open(csvPath, newline='') as csvfile:
            rows = csv.reader(csvfile)
            for row in rows:
                imgPath.append(row[0]) 
                labels.append(row[1])

        for i in range(len(imgPath)):
            img = cv2.imread(imgPath[i])
            img = cv2.resize(img, (112,112))
            if isAug:
                for j in range(5):
                    imgTensor = self.transform(img, j)
                    self.imgList.append(imgTensor)
                    self.labelList.append(int(labels[i]))
            else:
                imgTensor = self.transform(img, 3)
                self.imgList.append(imgTensor)
                self.labelList.append(int(labels[i]))

        imgPath.clear()
        labels.clear()
        del self.transform

    def __getitem__(self, index):
        return self.imgList[index], self.labelList[index]
        
    def __len__(self):
        return len(self.labelList)

def set_parameter_requires_grad(model, featureExtracting):
    if featureExtracting:
        for param in model.parameters():
            param.requires_grad = False
                
def VGG16_pretrained_model(numClasses, featureExtract=True, usePretrained=True):
    model = models.vgg16(pretrained=True)
    set_parameter_requires_grad(model, featureExtract)
    numFtrs = model.classifier[6].in_features
    model.classifier[6] = nn.Linear(numFtrs,numClasses)
    return model

class trainer: 
    def __init__(self, batch_size, epoch) :
        self.csv = "./csv/"
        self.batch_size = batch_size
        self.epoch = epoch
        self.lr = 0.0001
        self.loss = nn.CrossEntropyLoss()
        self.net = VGG16_pretrained_model(6).to(device)
        self.optimizer = torch.optim.Adam(self.net.parameters(), lr=self.lr)
        torch.backends.cudnn.benchmark = True

    def data_prepare(self):
        csvPath = self.csv
        for f in os.listdir(csvPath):
            if "train" in f:
                self.trainCsv = csvPath + f 
            elif "val" in f:
                self.valCsv = csvPath + f 

        print('Train CSV: ', self.trainCsv)
        print('Vald CSV : ', self.valCsv)

    def torch_dataset(self):
        self.trainDataset = ImageDataset(self.trainCsv, True)
        self.valDataset = ImageDataset(self.valCsv, False)

    def torch_dataLoader(self):
        self.dataLoaderTrain = torch.utils.data.DataLoader(  self.trainDataset,
                                                        batch_size = self.batch_size,
                                                        shuffle = True,
                                                        num_workers = 0,
                                                        drop_last = True)

        self.dataLoaderVal = torch.utils.data.DataLoader(self.valDataset,
                                                    batch_size = self.batch_size,
                                                    shuffle = True,
                                                    num_workers = 0,
                                                    drop_last = True)

    def fit(self):
        self.data_prepare()
        self.torch_dataset()
        self.torch_dataLoader()
        self.loss = self.loss.to(device)
        # begin train
        for i in range(self.epoch):
            self.net.train()
            beginTime = time.time()
            trainLosses = []
            valLosses = []
            for trainImage, trainLabel in self.dataLoaderTrain:
                self.optimizer.zero_grad()
                trainImage = trainImage.to(device)
                trainLabel = trainLabel.to(device, dtype=torch.long)
                output = self.net(trainImage)
                loss = self.loss(output, trainLabel)
                trainLosses.append(loss.detach().cpu().numpy())
                loss.backward()
                self.optimizer.step()

            self.net.eval()
            for valImage, valLabel in self.dataLoaderVal:
                with torch.no_grad():
                    valImage = valImage.to(device)
                    valLabel = valLabel.to(device, dtype=torch.long)
                    output = self.net(valImage)
                    loss = self.loss(output, valLabel)
                    valLosses.append(loss.detach().cpu().numpy())

            endTime = time.time()
            print(i, "Train : ", np.mean(trainLosses))
            print(i, "vald : ", np.mean(valLosses))
            print(i , "epoch time : ", (endTime - beginTime))
            torch.save(self.net, "./model/hair.pth")

if __name__ == '__main__':
    module = trainer(64, 30)
    module.fit()