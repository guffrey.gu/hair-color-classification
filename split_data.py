import os
import random
import numpy as np

def split_data(imagePath):
    label_dict = {'Bald':0,'Black_Hair': 1, 'Blond_Hair': 2,  
                'Brown_Hair': 3, 'Gray_Hair': 4, 'Receding_Hairline': 5}

    imgPathList = os.listdir(imagePath)

    allImgList = []
    allLabelList = []
    randomList = []

    trainImgList = []
    trainLabelList = []

    valImgList = []
    valLabelList = []

    for f in imgPathList:
        for imgName in os.listdir( imagePath + f ):
            img = imagePath + f + '/' + imgName
            allImgList.append(img)
            allLabelList.append(label_dict[f])

    while(1):
        rint = random.randint(0,len(allImgList))
        if rint not in randomList:
            randomList.append(rint)
        if len(randomList)> len(allImgList)*0.2:
            break

    for i in range(len(allImgList)):
        if i in randomList:
            valImgList.append([allImgList[i], allLabelList[i]])
        else:
            trainImgList.append([allImgList[i], allLabelList[i]])

    np.savetxt("./csv/train.csv", trainImgList, fmt="%s", delimiter=",")
    np.savetxt("./csv/val.csv", valImgList, fmt="%s", delimiter=",")

if __name__ == '__main__':
    split_data("./train/train/")