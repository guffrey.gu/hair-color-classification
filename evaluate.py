import numpy as np
import csv
import cv2
import os
import random
import time
import torch
from torch.utils.data.dataset import Dataset
import torch.nn as nn
import torch.nn.functional as F
import torchvision.models as models
from imgaug_transform import *
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class ImageDataset(Dataset):
    def __init__(self, csvPath, isAug):
        super().__init__()
        self.imgList = []
        self.labelList = []
        imgPath = []
        labels = []
        self.transform = ImgAugTransform()
        with open(csvPath, newline='') as csvfile:
            rows = csv.reader(csvfile)
            for row in rows:
                imgPath.append(row[0]) 
                labels.append(row[1])

        for i in range(len(imgPath)):
            img = cv2.imread(imgPath[i])
            img = cv2.resize(img, (112,112))
            if isAug == False:
                imgTensor = self.transform(img, 3)
                self.imgList.append(imgTensor)
                self.labelList.append(int(labels[i]))
        
        imgPath.clear()
        labels.clear()
        del self.transform

    def __getitem__(self, index):
        return self.imgList[index], self.labelList[index]
        
    def __len__(self):
        return len(self.labelList)

class evaluater:
    def __init__(self, batch_size):
        self.csv = "./csv/"
        self.lr = 0.0001
        self.batch_size = batch_size
        self.net = torch.load("./model/hair.pth").to(device)
        torch.backends.cudnn.benchmark = True

    def data_prepare(self):
        csvPath = self.csv
        for f in os.listdir(csvPath):
            if "train" in f:
                self.trainCsv = csvPath + f 
            elif "val" in f:
                self.valCsv = csvPath + f 

        print('Train CSV: ', self.trainCsv)
        print('Vald CSV : ', self.valCsv)

    def torch_dataset(self):
        self.trainDataset = ImageDataset(self.trainCsv, False)
        self.valDataset = ImageDataset(self.valCsv, False)
        
    def torch_dataLoder(self):
        self.dataLoaderTrain = torch.utils.data.DataLoader(  self.trainDataset,
                                                        batch_size = self.batch_size,
                                                        shuffle = False,
                                                        num_workers = 0,
                                                        drop_last = False)

        self.dataLoaderVal = torch.utils.data.DataLoader(self.valDataset,
                                                    batch_size = self.batch_size,
                                                    shuffle = False,
                                                    num_workers = 0,
                                                    drop_last = False)
    
    def evaluate(self, net, loader):
        net.eval()
        accuracy = 0
        count = 0
        for x, label in loader:
            x = x.to(device)
            label = label.to(device, dtype=torch.long)
            output = net(x)
            _, predicted = torch.max(output.data, 1)
            count += len(x)
            accuracy += (predicted == label).sum().item()
        return (accuracy / count)
        
    def fit(self):
        self.data_prepare()
        self.torch_dataset()
        self.torch_dataLoder()

        accuracyTrain = self.evaluate(self.net, self.dataLoaderTrain)
        accuracyVald = self.evaluate(self.net, self.dataLoaderVal)

        with open("./log/eval.txt", "w") as f:
            f.write("input size: {} \n\n".format(112))
            f.write("classes number: {} \n\n".format(6))
            f.write("use pretrained: {} \n\n".format(True))
            f.write("epochs: {} \n\n".format(30))
            f.write("batch size: {} \n\n".format(64))
            f.write("Train Accuracy: {} \n\n".format(accuracyTrain))
            f.write("vald Accuracy: {} \n\n".format(accuracyVald))

if __name__ == '__main__':
    evaluater = evaluater(64)
    evaluater.fit()