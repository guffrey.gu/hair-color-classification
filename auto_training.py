import yaml
import subprocess

with open("dvc.yaml", 'r') as f:
    params = yaml.safe_load(f)

subprocess.call("%s"%params['stages']['data']['cmd'])
subprocess.call("%s"%params['stages']['training']['cmd'])
subprocess.call("%s"%params['stages']['evaluate']['cmd'])